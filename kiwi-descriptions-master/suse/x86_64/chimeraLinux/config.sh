#!/bin/bash
#================
# FILE          : config.sh
#----------------
# PROJECT       : OpenSuSE KIWI Image System
# COPYRIGHT     : (c) 2006 SUSE LINUX Products GmbH. All rights reserved
#               :
# AUTHOR        : Marcus Schaefer <ms@suse.de>
#               :
# BELONGS TO    : Operating System images
#               :
# DESCRIPTION   : configuration script for SUSE based
#               : operating systems
#               :
#               :
# STATUS        : BETA
#----------------
#======================================
# Functions...
#--------------------------------------
test -f /.kconfig && . /.kconfig
test -f /.profile && . /.profile

#======================================
# Greeting...
#--------------------------------------
echo "Configure image: [$kiwi_iname]..."

#======================================
# Mount system filesystems
#--------------------------------------
baseMount

#======================================
# Setup baseproduct link
#--------------------------------------
suseSetupProduct

#======================================
# Add missing gpg keys to rpm
#--------------------------------------
suseImportBuildKey

#======================================
# Activate services
#--------------------------------------
suseInsertService sshd

#machine shit
suseInsertService systemd-nspawn@ubuntu

suseInsertService chroots



if [[ ${kiwi_type} =~ oem|vmx ]];then
    suseInsertService grub_config
else
    suseRemoveService grub_config
fi

#======================================
# Setup default target, multi-user
#--------------------------------------
baseSetRunlevel 5

#==========================================
# remove package docs
#------------------------------------------
rm -rf /usr/share/doc/packages/*
rm -rf /usr/share/doc/manual/*
rm -rf /opt/kde*

#Chimera Shit
#############################################
which pip3 &>> /NEW
pip3 install pytest-runner &>> /NEW
pip3 install wcwidth &>> /NEW
pip3 install ewmh &>> /NEW
pip3 install flake8 &>> /NEW
#pip3 install readchar &>> /NEW does not work... :(
#setup pyte
cd /usr/share/tmp/pyte/
python3 ./setup.py install &>> /NEW
cd -
#setup readchar
cd /usr/share/tmp/python-readchar-master
python3 ./setup.py install &>> /NEW
cd -

ln -s /usr/share/janit/janit.py /usb/bin/janit

ln -s /usr/share/janit/bin/openJanit.sh /usr/bin/openJanit


#inflate chroots
cd /chroots
#tar -xvzf ./*.tar.gz
tar -xvzf ./chimeraArch.tar.gz
tar -xvzf ./chimeraUbuntu.tar.gz
rm /chroots/*tar.gz
ln -s /chroots/* /var/lib/machines
cd -

#this is done by setupChroots.sh (called via systemd)
#setup ubuntu booting 
systemctl enable systemd-nspawn@ubuntu
systemctl enable systemd-nspawn@arch
#systemctl status systemd-nspawn@ubuntu

#systemctl start systemd-nspawn@ubuntu.service
systemctl enable chroots.service

#bug with ubuntu installer (disks in use) 
systemctl disable multipathd
#======================================
# Thanks GeckoLinux!!!
#--------------------------------------
rm -R /etc/skel/bin
rm -R /etc/skel/public_html
#rm /usr/share/fonts/truetype/Ubuntu-M.ttf
#rm /usr/share/fonts/truetype/Ubuntu-MI.ttf


## Fix gvfs-trash functionality for live session user
#mkdir /.Trash-1999
#chown 1999:users /.Trash-1999
#chmod 700 /.Trash-1999
for i in NetworkManager tlp tlp-sleep avahi-dnsconfd; do
	systemctl -f enable $i
done
for i in purge-kernels wicked auditd apparmor; do
	systemctl -f disable $i
done

service NetworkManager start

/usr/sbin/useradd -m -d /home/tmp tmp
echo -e "tmp\ntmp\n" | passwd tmp
chown -R tmp:users /home/tmp
ln -s /usr/lib/systemd/system/runlevel5.target /etc/systemd/system/default.target
baseSetRunlevel 5

ln -s /usr/share/janit/janit.py /usr/bin/janit


#fix timezone bug on install
rm /etc/localtime
chmod +x /home/tmp/Desktop/Installer_passwd_is_root.desktop

#======================================
# only basic version of vim is
# installed; no syntax highlighting
#--------------------------------------
sed -i -e's/^syntax on/" syntax on/' /etc/vimrc

#======================================
# SuSEconfig
#--------------------------------------
#suseConfig

#======================================
# Remove yast if not in use
#--------------------------------------
#suseRemoveYaST

#======================================
# Umount kernel filesystems
#--------------------------------------
#baseCleanMount

exit 0
