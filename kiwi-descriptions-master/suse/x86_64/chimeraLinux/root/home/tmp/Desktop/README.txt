Chimera OS
The Linux distribution of Linux distributions
by: David Hamner 
email: ke7oxh@gmail.com

Anything I made is GPL3, Have a lot of fun!
Source0: https://bitbucket.org/hackersgame/chimeraos
Source1: https://bitbucket.org/hackersgame/janit
OBS project: https://build.opensuse.org/project/show/home:hackersgame


The menu is called janit ( Just Another Ncurses Interface Tool )


Janit:
  Navigation:
    ctrl + left/right to open/change terminal window (That’s the multiplexing part) 
    ctrl + up/down with switch between terminal window and menu system
    The <enter key> from the menu without a command will switch to the Terminal too
 
  Menu system:
    Run an application by entering:  name <tab to auto-complete>
    To see the menu of installed programs hit <tab> without text
      sub-menus can be seen by entering the sub-menu name 
      (the sub-menu names will be ignore when the command is run) 
    To select open window run : \<window name txt><tab to auto-complete> 
    Arrow left/right to switch desktops 
    run <distroName> to swich distros (done via systemd machines)

  TopBar:
    Top left number shows which terminal you are using
    Open windows are shown as: \Window1 \Window2 \Window3

  BottomBar: 
    Left in the input side.
    Right shows the virtual desktops (and which is active) 

  Target system;
    From the menu you can enter a path to a file (~ and ./ will be turned into an absolute path) 
      <tab> at this point will show the applications that can run the file type 
      Running a command will now pass the target into the program as an argument 
    Run “done” to unset the target 
    Run “next” to try to toggle to the next alike file.
      Example: a target of: “/home/Data/my Favorite Show/S01/e01.mp4” would become ../e02.mp4
    Run “back” to try to toggle to the previous alike file

Installation (not fully implemented yet):
  This is an online installer. (Internet is needed) 
  run: “installation” from the menu (passwd: root)
    This will install base OpenSuSE/Mate/Janit but will not include a bootable Arch or Ubuntu yet.

  
